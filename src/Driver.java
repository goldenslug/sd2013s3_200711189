//Driver.java

abstract class Automobile{
	
	abstract int getCapacity();
	
	abstract void move();
}

class Sedan extends Automobile{
	int capacity;
	public Sedan(int capacity){
		this.capacity = capacity;	
	}
		
	public int getCapacity(){
		return capacity;
	}

	public void move(){
		System.out.println("Sedan go");
	}
}

class Minivan extends Automobile{

	int capacity;
        public Minivan(int capacity){
                this.capacity = capacity;
        }

	public int getCapacity(){
                return capacity;
        }

	public void move(){
		System.out.println("Minivan gogo");
	}
}

class SportsCar extends Automobile{
	
	int capacity;
        public SportsCar(int capacity){
                this.capacity = capacity;
        }

	public int getCapacity(){
                return capacity;
        }

	public void move(){
		System.out.println("SportsCar gogogo");
	}
}

/*class Driver{
	public void moveCar(Sedan sedan){
		sedan.move();
	}
	public void moveCar(Minivan minivan){
		minivan.move();
	}
	public void moveCar(Sportscar sportscar){
		sportscar.move();
	}
}*/

class Driver{
	public void moveCar(Automobile am){
		am.move();
	}
}
