
import java.util.*;

public class Ch7IDCardFactory extends Ch7Factory {
	private Vector owners = new Vector();
	protected Ch7Product createProduct(String owner) {
		return new Ch7IDCard(owner);
	}
	protected void registerProduct(Ch7Product product) {
		owners.add(((Ch7IDCard)product).getOwner());
	}
	public Vector getOwners() {
		return owners;
	}
}

