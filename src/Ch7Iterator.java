public interface Ch7Iterator {
	public abstract boolean hasNext();
	public abstract Object next();
}

