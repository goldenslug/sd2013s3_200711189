public class Ch7Singleton {
	private static Ch7Singleton instance = null;
	private Ch7Singleton() {
		System.out.println("only one instance");
	}
	public static synchronized Ch7Singleton instance() {
		if (instance == null) {
			instance = new Ch7Singleton();
		}
		return instance;
	}
}

