public class Ch7GarageDoorCloseCommand implements Ch7Command{
	Ch7GarageDoor gDoor;
	public Ch7GarageDoorCloseCommand(Ch7GarageDoor garageDoor) {
		this.gDoor=garageDoor;
	}
	public void execute() {
		gDoor.down();
	}	
}

