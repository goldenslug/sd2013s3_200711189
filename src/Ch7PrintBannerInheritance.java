class Ch7PrintBannerInheritance implements Ch7Print {

	Ch7Banner ch;
	public Ch7PrintBannerInheritance(String string) {
		ch = new Ch7Banner(string);
	}
	public void printStrong() {
		ch.showWithAster();
	}
	public void printWeak() {
		ch.showWithParen();
	}
}
