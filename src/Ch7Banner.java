public class Ch7Banner {
	private String string;
	public Ch7Banner(String string) {
		this.string = string;
	}
	public void showWithParen() {
		System.out.println("("+string+")");
	}
	public void showWithAster() {
		System.out.println("*"+string+"*");
	}
}
