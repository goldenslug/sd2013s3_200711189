class Sorter_v1 { // 파란색 틀림
	public static void main(String[] args) {
		Integer[] numbers = {new Integer(19), new Integer(79), new Integer(2), new Integer(8)};
		String[] strings = {"Jin", "Adam", "Digital", "Sangmyung"};

		System.out.println("before=====================");
		for (int i = 0; i < numbers.length; i++) {
			System.out.println("numbers["+i+"]="+numbers[i]);
		}
		for (int i = 0; i < strings.length; i++) {
			System.out.println("strings["+i+"]="+strings[i]);
		}

		ch3Sorter.sort(numbers);
		ch3Sorter.sort(strings);

		System.out.println("after=====================");
		for (int i = 0; i < numbers.length; i++) {
			System.out.println("numbers["+i+"]="+numbers[i]);
		}

		for (int i = 0; i < strings.length; i++) {
			System.out.println("strings["+i+"]="+strings[i]);
		}
	}
}

class ch3Sorter {
	public static void sort(Integer[] data) {
		for (int i = data.length-1; i >= 1; i--) {
			int indexOfMax = 0;
			for (int j = 1; j <= i; j++) {
				if (data[j] > data[indexOfMax])
					indexOfMax = j;
			}
			Integer temp = data[i];
			data[i] = data[indexOfMax];
			data[indexOfMax] = temp;
		}
	}	
	public static void sort(String[] data) {
		for (int i = data.length-1; i >= 1; i--) {
			int indexOfMax = 0;
			for (int j = 1; j <= i; j++) {
				if (data[j].compareTo(data[indexOfMax]) > 0)
					indexOfMax = j;
			}

			String temp = data[i];
			data[i] = data[indexOfMax];
			data[indexOfMax] = temp;
		}
	}
}
