public class Ch7BookShelfIterator implements Ch7Iterator {
	private Ch7BookShelfCollection bookShelfCollection;
	private int index;

	public Ch7BookShelfIterator(Ch7BookShelfCollection bookShelf) {
		this.bookShelfCollection = bookShelf;
		this.index = 0;
	}

	public boolean hasNext() {
		if(index < bookShelfCollection.getLength()) {
			return true;
		} else {
			return false;
		}
	}
	public Object next() {
		Ch7Book book = bookShelfCollection.getBookAt(index);
		index++;
		return book;
	}
}

