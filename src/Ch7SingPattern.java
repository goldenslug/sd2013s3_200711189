public class Ch7SingPattern {
	private static  Ch7SingPattern uniqueInstance;
	public void writeLine(String text) {}
	public void readEntireLog() {
		System.out.println("Normal log goes here");
	}
	public static synchronized Ch7SingPattern instance(){
		if(uniqueInstance == null)
		{
			uniqueInstance = new Ch7SingPattern();
		}
		return uniqueInstance;
	}
}

