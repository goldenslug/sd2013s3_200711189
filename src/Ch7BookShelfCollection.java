import java.util.ArrayList;

public class Ch7BookShelfCollection implements Ch7Collection {
	//private Ch7Book[] books;
	private ArrayList<Ch7Book> books;
	private int last = 0;
		
	public Ch7BookShelfCollection(int maxsize) {
//		this.books = new Ch7Book[maxsize];
		books = new ArrayList<Ch7Book>(maxsize);

	}
	public Ch7Book getBookAt(int index) {
	//	return books[index];
		return books.get(index);
	}
	public void appendBook(Ch7Book book) {
	//	this.books[last] = book;
		books.add(book);
		last++;
	}
	public int getLength() {
		return last;
	}
	public Ch7Iterator iterator() {
		return new Ch7BookShelfIterator(this);
	}
}

