import java.util.*;
import java.awt.*;
import javax.swing.JFrame;
public class SD2013s3_200711189_Tester{
	public static void main(String[] args){
		/*ch2main();
		ch3main();	
		ch8main();
		ch9main();		
		ch4main();
//		ch4_1();
		ch4_2main();
		ch2_7main();*/
//		ch5_1main();
//		ch5_2main();
//		ch7_main();
//		ch7_main1();
//		ch7_main2();
//		ch7_main3();
		ch7_main4();
		ch7_main5();
		}
	private static void ch7_main5(){
		Ch7Factory factory = new Ch7IDCardFactory();
		Ch7Product card1 = factory.create("나소설");
		Ch7Product card2 = factory.create("너소설");
		Ch7Product card3 = factory.create("그소설");

		card1.use();
		card2.use();
		card3.use();
	}	
	private static void ch7_main4(){
		Ch7SimpleRemoteControl remote = new Ch7SimpleRemoteControl();
		Ch7Light light = new Ch7Light();
		Ch7LightOnCommand lightOn = new Ch7LightOnCommand(light);
		remote.setCommand(lightOn);
		remote.buttonWasPressed();

		Ch7GarageDoor gdoor = new Ch7GarageDoor();

		Ch7GarageDoorOpenCommand garageOpen = new Ch7GarageDoorOpenCommand(gdoor);
                remote.setCommand(garageOpen);
		remote.buttonWasPressed();

		Ch7LightOffCommand lightDown = new Ch7LightOffCommand(light);
		remote.setCommand(lightDown);
		remote.buttonWasPressed();

                Ch7GarageDoorCloseCommand garageDown = new Ch7GarageDoorCloseCommand(gdoor);
		
		remote.setCommand(garageDown);
                remote.buttonWasPressed();

}


	private static void ch7_main3(){
		Ch7BookShelfCollection bookShelfCollection = new Ch7BookShelfCollection(3);
		bookShelfCollection.appendBook(new Ch7Book("소프트웨어공학"));
		bookShelfCollection.appendBook(new Ch7Book("소프트웨어설계"));
		bookShelfCollection.appendBook(new Ch7Book("프로그래밍"));

		Ch7Iterator it = bookShelfCollection.iterator();
		while (it.hasNext()) {
			Ch7Book book = (Ch7Book) it.next();
			System.out.println(book.getName());
		}
	}
	private static void ch7_main2(){
		Ch7SingPattern loogerB = new Ch7SingPattern();
		loogerB.readEntireLog();
	}

	private static void ch7_main1(){
		System.out.println("Singleton Pattern");
		Ch7Singleton obj1 = Ch7Singleton.instance();
		Ch7Singleton obj2 = Ch7Singleton.instance();
		
	}
	private static void ch7_main(){
		Ch7Print p = new Ch7PrintBannerInheritance("Hello");
		p.printStrong();
		p.printWeak();
	}
	private static void ch5_2main(){
		System.out.println("ch5 - Immutable");
		Ch5FixedPointv1New ch5 = new Ch5FixedPointv1New(3,5);
		System.out.println(ch5.getX());
		System.out.println(ch5.getY());
		
		System.out.println("ch5 - mutable");
		Ch5FixedPointv2Inheritance ch5_3 = new Ch5FixedPointv2Inheritance(3,5);
		ch5_3.setLocation(5,6);
                System.out.println(ch5_3.getX());
		ch5_3.x=7;
                System.out.println(ch5_3.getY());
		System.out.println(ch5_3.getX());

		System.out.println("ver1");
                FixedPoint fp1 = new FixedPoint(3, 4);
                System.out.println(fp1.getX()); //prints 3
                Point loc = fp1.getLocation();
                loc.x = 5;
                System.out.println(fp1.getX()); //prints 5

                System.out.println("ver2");
                Point p = new Point(3, 4);
                FixedPoint fp2 = new FixedPoint(p);
                System.out.println(fp2.getX()); //prints 3
                p.x = 5;
                System.out.println(fp2.getX()); //prints 5

	}
	private static void ch5_1main(){
		// String class is ‘Immutable’
		String s = "sm";
		System.out.println("s = "+s);
		System.out.println("s hashcode = "+System.identityHashCode(s));
		s = s.concat("univ");
		System.out.println("s (concat) ="+s);
		System.out.println("s (concat) hashcode = "+System.identityHashCode(s));
		s = s.replace(s, "smu");
		System.out.println("s (replace) ="+s);
                System.out.println("s (replace) hashcode = "+System.identityHashCode(s));

		// concat(추가문자열:String), replace(변경될문자열:String, 변경문자열:String) in String class

		// StringBuffer class is ‘mutable’
		StringBuffer sb = new StringBuffer("sm");
		System.out.println("sb = "+sb);
		System.out.println("sb hashcode = "+System.identityHashCode(sb));
		sb.append("univ");
		System.out.println("sb(append) = "+sb);
		System.out.println("sb(append) hashcode = "+System.identityHashCode(sb));

		// append(추가문자열:String) in StringBuffer class
	}
	private static void ch2_7main(){
		System.out.println("-----ch2_7 Driver------");
		Driver d = new Driver();
		d.moveCar(new Sedan(0));
		d.moveCar(new Minivan(0));
		d.moveCar(new SportsCar(0));		
               
	}
	// jmkim. 20130923 다형성을 보기 위한 Version2 코드가 없구나.
	 private static void ch4main(){
	  System.out.println("-----------ch4main--------------");
	  Automobile[] fleet = new Automobile[3];
                fleet[0] = new Sedan(5);
                fleet[1] = new Minivan(7);
                fleet[2] = new SportsCar(2);
	   int totalCapacity = 0;
                for(int i=0; i<fleet.length; i++){
                        if(fleet[i] instanceof Sedan)
                                totalCapacity += fleet[i].getCapacity();
                        else if(fleet[i] instanceof SportsCar)
                                totalCapacity += fleet[i].getCapacity();
                        else if(fleet[i] instanceof Minivan)
                                totalCapacity += fleet[i].getCapacity();
                        else
                                totalCapacity += fleet[i].getCapacity();
                }
                System.out.println(totalCapacity);
	}

	private static void ch2_6main(){
		System.out.println("interface");
		//Colorable c = new Colorable();객체화 불가능
		Colorable pt = new PlainText();
		Colorable ct= new CompoundText();
		Colorable cc = new Circle();
		
		Changeable cc2 = new Circle();
		//Colorable cr4 = new Changeable(); 둘다 인터페이스이기 때문
	}
	
	private static void ch2_5main(){
		Account ac = new Account();
		Account ca = new CheckingAccount();
		Account cla = new CreditLineAccount();
		Account ctca = new CheckingTrafficCardAccount();
		//CheckingAccount ac2 = new Account();
         	//CheckingAccount cla2 = new CreditLineAccount();
	}

	private static void ch2main(){
		System.out.println("----ch2 Person---");
		Person FP = new Person("pmw",new Date(1988-1900,7,27));
                String rName = FP.getName();

                System.out.println("이름:"+rName+" "+"날짜:"+FP.getBirthdate());

	}

	private static void ch3main(){
	
		System.out.println("-----ch3 Person---");
		SimpleRunner sr = new SimpleRunner();
		sr.run();
		// test
		Runnable ra = new SimpleRunner();
		ra.run();
		//ra.eat();
	}

  

	private static void ch8main(){
                System.out.println("-- ch8 Overload --");

                Object2 o = new Object2();
                Automobile2 auto = new Automobile2();
                Object2 autoObject = new Automobile2();

                auto.equals2(o);
                auto.equals2(auto);
                auto.equals2(autoObject);
		
		System.out.println("===================================");
		o.equals2(o);
		o.equals2(auto);
		o.equals2(autoObject);
		autoObject.equals(o);
		autoObject.equals2(auto);
                autoObject.equals2(autoObject);

        }
	private static void ch9main(){
                System.out.println("-- ch9 Override--");
                
                Object3 o = new Object3();
               	Automobile3 auto = new Automobile3();
                Object3 autoObject = new Automobile3();
                auto.equals3(o);
                auto.equals3(auto);
                auto.equals3(autoObject);

                o.equals3(o);
                o.equals3(auto);
                o.equals3(autoObject);
                autoObject.equals(o);
                autoObject.equals3(auto);
                autoObject.equals3(autoObject);

        }

	private static void ch4_2main() {
		//---(1) Ch4Triangle version 1 equals() 테스트

		System.out.println("===Ch4Triangle equals() version 1 #1===");

		Ch4Triangle t1 = new Ch4Triangle(new Point(0,0), new Point(1,1),new Point(2,2));

		Ch4ColoredTriangle rct1 = new Ch4ColoredTriangle(Color.red,new Point(0,0), new Point(1,1),new Point(2,2));

		System.out.println("t1.equals(rct1): "+t1.equals(rct1));
		System.out.println("rct1.equals(t1): "+rct1.equals(t1));
		//---(1-1) (1) 문제점 확인
		//---색이 틀린데, true가 찍혀. symmetric, transitive, consistent 만족하>지만... 
		System.out.println("===Ch4Triangle equals() version 1 #2===");
		Ch4ColoredTriangle rct1_1 = new Ch4ColoredTriangle(Color.red,new Point(0,0), new Point(1,1), new Point(2,2));
		Ch4ColoredTriangle bct1_1 = new Ch4ColoredTriangle(Color.blue,new Point(0,0), new Point(1,1),new Point(2,2));
		System.out.println("rct1_1.equals(bct1_1): "+rct1_1.equals(bct1_1));
		System.out.println("bct1_1.equals(rct1_1): "+bct1_1.equals(rct1_1));
		//
		//---(2) 위 문제점 해결 Ch4ColoredTriangle version 1 equals() 테스트
		System.out.println("===Ch4Triangle + Ch4ColoredTriangle equals() version 1===");
		Ch4Triangle t2 = new Ch4ColoredTriangle(null, new Point(0,0), new Point(1,1), new Point(2,2));
		Ch4ColoredTriangle rct2 = new Ch4ColoredTriangle(Color.red,new Point(0,0), new Point(1,1),new Point(2,2));
		System.out.println("t2.equals(rct2): "+t2.equals(rct2));
		System.out.println("rct2.equals(t2): "+rct2.equals(t2));


		//---(3) Ch4ColoredTriangle version 2 equals() 테스트
		//---(4) test
		System.out.println("===Ch4Triangle + Ch4ColoredTriangle equals() version 2===");
		Ch4Triangle t3 = new Ch4Triangle(new Point(0,0), new Point(1,1),new Point(2,2));
		Ch4ColoredTriangle rct3 = new Ch4ColoredTriangle(Color.red,new Point(0,0), new Point(1,1),new Point(2,2));
		Ch4ColoredTriangle bct3 = new Ch4ColoredTriangle(Color.blue,new Point(0,0), new Point(1,1),new Point(2,2));
		System.out.println("rct3.equals(t3): "+rct3.equals(t3));
		System.out.println("t3.equals(bct3): "+t3.equals(bct3));
		System.out.println("rct3.equals(bct3): "+rct3.equals(bct3));

		/////////////////////////////////////////////////////////////////////////
		System.out.println("t1.equals(t3): "+t1.equals(t3));
		System.out.println("t2.equals(t3): "+t2.equals(t3));
	}

}
