public class Ch7SimpleRemoteControl {
	Ch7Command slot;

	public Ch7SimpleRemoteControl() {}

	public void setCommand(Ch7Command command) {
		slot = command;
	}

	public void buttonWasPressed() {
		slot.execute();
	}

}

