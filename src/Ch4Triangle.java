import java.awt.Color;
import java.awt.Point;

class Ch4Triangle {
        private Point p1, p2, p3;
        public Ch4Triangle(Point p1, Point p2, Point p3) {
                if (p1 == null)
                        p1 = new Point(0,0);
                if (p2 == null)
                        p2 = new Point(0,0);
                if (p3 == null)
                        p3 = new Point(0,0);
                this.p1 = p1;
                this.p2 = p2;
                this.p3 = p3;
        }
       /* //---version 1 (1), (1-1)
        public boolean equals(Object obj) {
                if(!(obj instanceof Ch4Triangle))
                        return false;
                Ch4Triangle otherCh4Triangle = (Ch4Triangle) obj;
                return (p1.equals(otherCh4Triangle.p1) && 
                        p2.equals(otherCh4Triangle.p2) &&
                        p3.equals(otherCh4Triangle.p3));
        }
	*/
	//---version 2 (4)
        public boolean equals(Object obj) {
                if(obj == null)
                        return false;
                if(obj.getClass() != this.getClass())
                        return false;
                Ch4Triangle otherCh4Triangle = (Ch4Triangle) obj;
                return (p1.equals(otherCh4Triangle.p1) &&
                        p2.equals(otherCh4Triangle.p2) &&
                        p3.equals(otherCh4Triangle.p3));
        }
        
}

class Ch4ColoredTriangle extends Ch4Triangle {
        private Color color;
        public Ch4ColoredTriangle(Color c, Point p1, Point p2, Point p3) {
                super(p1, p2, p3);
                if (c == null)
                        c = Color.red;
                color = c;
        }
        //---version 1 (2)
        public boolean equals(Object obj) {
                if(!(obj instanceof Ch4ColoredTriangle))
                        return false;
                Ch4ColoredTriangle otherCh4ColoredTriangle = (Ch4ColoredTriangle) obj;
                return super.equals(otherCh4ColoredTriangle) &&
                        this.color.equals(otherCh4ColoredTriangle.color);
        }
        //

        /*/---version 2 (3) 
        public boolean equals(Object obj) {
                if(obj instanceof Ch4ColoredTriangle){
                        Ch4ColoredTriangle otherCh4ColoredTriangle = (Ch4ColoredTriangle) obj;
                        return super.equals(otherCh4ColoredTriangle) &&
                              this.color.equals(otherCh4ColoredTriangle.color);
                }
                else if(obj instanceof Ch4Triangle) {
                        return super.equals(obj);
                }
                else return false;
        }
        /*

        //---version 3 (4)
        public boolean equals(Object obj) {
            if(obj == null)
	    return false;
            if(obj.getClass() != this.getClass())
	    return false;
            if(!super.equals(obj))
	    return false;
            Ch4ColoredTriangle otherCh4ColoredTriangle = (Ch4ColoredTriangle) obj;
            return this.color.equals(otherCh4ColoredTriangle.color);
        }
        */
}
