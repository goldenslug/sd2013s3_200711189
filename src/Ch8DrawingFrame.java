import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToolBar;

public class Ch8DrawingFrame extends JFrame {
	public Ch8DrawingFrame() {
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		JComponent drawingCanvas=createDrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);
		JToolBar toolbar=createToolbar();
		add(toolbar, BorderLayout.NORTH);
	}

	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas=new JToolBar();
		drawingCanvas.setPreferredSize(new Dimension(400, 300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}
	private JToolBar createToolbar() {
		JToolBar toolbar=new JToolBar();
		JButton ellipseButton=new JButton("Ellipse");
		toolbar.add(ellipseButton);
		JButton squareButton=new JButton("Square");
		toolbar.add(squareButton);
		JButton rectButton=new JButton("Rect");
		toolbar.add(rectButton);
		return toolbar;
	}
	public static void main(String[] args) {
		Ch8DrawingFrame drawFrame=new Ch8DrawingFrame();
		drawFrame.pack();
		drawFrame.setVisible(true);
	}
}
