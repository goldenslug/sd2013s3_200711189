public class SoldOutState implements State {
	GumballMachine gumballMachine;
    public SoldOutState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }
	public void insertQuarter() {
                System.out.println("매진되었습니다, 다음기회를 이용해주세요");
                gumballMachine.setState(gumballMachine.getHasQuarterState());
        }
        // 동전 반환
        public void ejectQuarter() {
                System.out.println("동전을 넣지 않으셨습니다. 동전이 반환되지 않습니다.");
        }
        // 손잡이 돌리기
        public void turnCrank() {
                System.out.println("매진되었습니다.");
         }
        // 알맹이 꺼내기
        public void dispense() {
                System.out.println("매진입니다.");
        }
	
	public String toString() {
                return "waiting for quarter";
        }

}
