import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToolBar;

public class Ch8DrawingFrame extends JFrame {
	public Ch8DrawingFrame() {
		super("Drawing Application");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		JComponent drawingCanvas=createDrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);
		JToolBar toolbar=createToolbar(drawingCanvas);
		add(toolbar, BorderLayout.NORTH);
	}

	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas=new JToolBar();
		drawingCanvas.setPreferredSize(new Dimension(400, 300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		return drawingCanvas;
	}
	private JToolBar createToolbar(JComponent canvas) {
		JToolBar toolbar = new JToolBar();
        //add the buttons to the toolbar
        	JButton ellipseButton = new JButton("Ellipse");
        	toolbar.add(ellipseButton);
       		JButton squareButton = new JButton("Square");
       		toolbar.add(squareButton);
        	JButton rectButton = new JButton("Rect");
        	toolbar.add(rectButton);

        //add the CanvasEditor listener to the canvas and to the buttons,
        //with the ellipseButton initially selected
        	CanvasEditor canvasEditor = new CanvasEditor(ellipseButton);
        	ellipseButton.addActionListener(canvasEditor);
        	squareButton.addActionListener(canvasEditor);
        	rectButton.addActionListener(canvasEditor);
        	canvas.addMouseListener(canvasEditor);
		return toolbar;
	}


	public static void main(String[] args) {
		Ch8DrawingFrame drawFrame=new Ch8DrawingFrame();
		drawFrame.pack();
		drawFrame.setVisible(true);
	}
}
