import javax.swing.*;
import java.awt.*;

public class Ch8SimpleDrawMain extends JFrame {
	public Ch8SimpleDrawMain() {
		setSize(200, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public static void main(String[] args) {
		Ch8SimpleDrawMain drawFrame = new Ch8SimpleDrawMain();
		drawFrame.setVisible(true);
	}
	public void paint(Graphics g) {
		g.drawRect(10, 50, 10, 10);
		g.drawOval(10, 50, 20, 30);
		g.setColor(Color.red);
		g.fill3DRect(40,40,40,40,true);
	}
}
