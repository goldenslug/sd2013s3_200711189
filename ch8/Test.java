public class Test {
	public static void main(String[] args) {
		ComplexClass cc = new ComplexClass("상명대학교", "디지털미디어학부", "3학년");
		try{
			ComplexClass cc1 = (ComplexClass)cc.clone();
			ComplexClass cc2 = (ComplexClass)cc.clone();
			//
			cc1.clone();
			cc.setName("park");
			System.out.println(cc.getName());
			cc2.clone();
			cc.setName("lee");
			System.out.println(cc.getName());
			
		} catch (CloneNotSupportedException e) {

			e.printStackTrace();
		}
	}
}

