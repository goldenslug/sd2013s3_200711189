<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true"%>

<%
	session.setAttribute("sess1", "세션 스트링 테스트");
	session.setAttribute("sess2", new Integer(1));
	session.setAttribute("sess3", new Double(3.14));
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>Insert title here</title>
</head>
<body>
	<h2>세션변수가 저장되었습니다.</h2>
	<h3>확인해 보세요.</h3>
	<a href="session2.jsp">여기</a>
</body>
</html>
