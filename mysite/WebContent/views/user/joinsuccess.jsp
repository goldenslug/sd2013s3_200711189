<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<title>RFM Project</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/mysite/assets/css/user.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="container">
		<div id="header">
			<jsp:include page ="/views/include/header.jsp" flush="true"/>
		</div>
		<div id="content">
			<div id="user">
				<p>
					회원가입을 축하합니다.<br>
					<a href="/mysite/user?a=loginform">로그인하기</a>
				</p>				
			</div>
		</div>
		<div id="navigation">
			<ul>
				<li><a href="/mysite/views/main/index.jsp">Home</a></li>
				<li><a href="/mysite/views/guestbook/list.jsp">방명록</a></li>
			</ul>
		</div>
		<div id="footer">
			<p>(c)opyright 2013 </p>
		</div>
	</div>
</body>
</html>