<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String result = request.getParameter("reuslt");
%>
<!doctype html>
<html>
<head>
<title>mysite</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/mysite/assets/css/user.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="container">
		<div id="header">
			<jsp:include page ="/views/include/header.jsp" flush="true"/>
		</div>
		<div id="content">
			<div id="user">

				<form id="login-form" name="loginform" method="post" action="/mysite/user" >
				 	<input type="hidden" name="a" value="login">
					<label class="block-label" for="email">이메일</label>
					<input id="email" name="email" type="text" value="">
					
					<label class="block-label" >패스워드</label>
					<input name="password" type="password" value="">
					<%
						if("fail".equals(result)){
					%>
					<p>
						로그인이 실패 했습니다.
					</p>
					<%
					}
					%>
					<input type="submit" value="로그인">
					
				</form>
			</div>
		</div>
		<div id="navigation">
			<ul>
				<li><a href="/mysite/views/main/index.jsp">Home</a></li>
				<li><a href="/mysite/views/guestbook/list.jsp">방명록</a></li>
			</ul>
		</div>
		<div id="footer">
			<p>(c)opyright 2013 </p>
		</div>
	</div>
</body>
</html>