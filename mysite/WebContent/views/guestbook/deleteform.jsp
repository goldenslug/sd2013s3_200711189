<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("utf-8");
	String no = request.getParameter("no");
%>
<!doctype html>
<html>
<head>
<title>mysite</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/mysite/assets/css/guestbook.css" rel="stylesheet" type="text/css">
</head>
<body>

<div id="container">
		<div id="header">
			<jsp:include page ="/views/include/header.jsp" flush="true"/>
		</div>
		<div id="content">
			<div id="guestbook" class="delete-form">
				<form method="post" action="/mysite/user?a=delete">
					<input type='hidden' name="no" value="<%=no%>">
					<label>비밀번호</label>
					<input type="password" name="pass">
					<input type="submit" value="확인">
				</form>
				<a href="/mysite/views/guestbook/list.jsp">방명록 리스트</a>
			</div>
		</div>
		<div id="navigation">
			<ul>
				<li><a href="/mysite/views/main/index.jsp">박민욱</a></li>
				<li class="selected"><a href="/mysite/views/guestbook/list.jsp">방명록</a></li>
			</ul>
		</div>
		<div id="footer">
			<p>(c)opyright 2013 </p>
		</div>
	</div>
</body>
</html>