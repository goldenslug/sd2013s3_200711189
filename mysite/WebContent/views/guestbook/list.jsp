<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="guestbook.GuestDao"%>
<%@ page import="guestbook.GuestVo"%>
<%
	GuestDao dao = new GuestDao();
	List<GuestVo> list = dao.getList();
%>
<!doctype html>
<html>
<head>
<title>mysite</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link href="/mysite/assets/css/guestbook.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="container">
		<div id="header">
			<jsp:include page="/views/include/header.jsp" flush="true"/>
		</div>

		<div id="content">
			<div id="guestbook">
				
				<form action="/mysite/user?a=add" method="post">
					<table>
						<tr>
							<td>이름</td><td><input type="text" name="name"></td>
							<td>비밀번호</td><td><input type="password" name="pass"></td>
						</tr>
						<tr>
							<td colspan=4><textarea name="memo" cols=60 rows=5></textarea></td>
						</tr>
						<tr>
							<td colspan=4 align=right><input type="submit" VALUE=" 확인 "></td>
						</tr>
					</table>
				</form>
				<ul>
					<li>
					<%
				int size = list.size();
				for (int i = 0; i < size; i++) {
					GuestVo v = list.get(i);
			%>
						<table>
							<tr>
								<td><%=v.getNo() %></td>
								<td><%=v.getName() %></td>
								<td><%=v.getDate() %></td>
								<td><a href="/mysite/user?a=deleteform&no=<%=v.getNo() %>">삭제</a></td>
							</tr>
							<tr>
								<td colspan="4">
									<%=v.getMemo() %>
								</td>
							</tr>
						</table>
						<%
				}
			%>
					</li>
				</ul>
			</div>
		</div>
		<div id="navigation">
			<ul>
				<li><a href="/mysite/views/main/index.jsp">박민욱</a></li>
				<li class="selected"><a href="/mysite/views/guestbook/list.jsp">방명록</a></li>
			</ul>
		</div>
		<div id="footer">
			<p>(c)opyright 2013 </p>
		</div>
	</div>
</body>
</html>