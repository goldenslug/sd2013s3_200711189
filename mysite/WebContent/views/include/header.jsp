<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="kr.ac.smu.vo.UserVo" %>
<%
	session = request.getSession(false);
	UserVo vo = (UserVo)session.getAttribute("authUser");
%>
<h1>RFM Project</h1>
<ul>
	<%
	if(vo == null){ 
	%>
		<li><a href="/mysite/user?a=loginform">로그인</a><li>
		<li><a href="/mysite/user?a=joinform">회원가입</a><li>
	<%}else{ %>
		
		<li><a href="/mysite/user?a=logout">로그아웃</a><li>
		<li><p><%=vo.getName() %>님 안녕하세요 ^^;<p><li>
	<%} %>
</ul>