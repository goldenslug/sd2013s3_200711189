package kr.ac.smu.mvc.action.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.dao.UserDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.UserVo;

public class JoinAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		
		String name =request.getParameter("name");
		String email =request.getParameter("email");
		String password =request.getParameter("password");
		String gender = request.getParameter("gender");
		
		UserVo vo = new UserVo();
		vo.setName(name);
		vo.setEmail(email);
		vo.setPassword(password);
		vo.setGender(gender);
		
		UserDao dao = new UserDao();
		dao.insert(vo);
		System.out.println("넘오냐?");
		HttpUtil.redirect(request,response, "/mysite/user?a=joinsuccess");
	}

}
