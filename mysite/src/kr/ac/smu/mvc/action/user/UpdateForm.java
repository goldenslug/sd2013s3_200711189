package kr.ac.smu.mvc.action.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.BoardDao;
import board.BoardVo;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;

public class UpdateForm extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String no = request.getParameter("no");
		BoardDao dao = new BoardDao();
		BoardVo vo  = dao.getBoard(no);
		request.setAttribute("values", vo);
		HttpUtil.forward(request, response, "/views/board/update.jsp");
	}

}
