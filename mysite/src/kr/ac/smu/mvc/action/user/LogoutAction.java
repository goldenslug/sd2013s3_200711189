package kr.ac.smu.mvc.action.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;

public class LogoutAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession(false);
		session.removeAttribute("authUser");
		session.invalidate();
			
		HttpUtil.redirect(request,response, "/mysite/main");
	}

}
