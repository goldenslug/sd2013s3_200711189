package kr.ac.smu.mvc.action.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kr.ac.smu.dao.UserDao;
import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.vo.UserVo;

public class LoginAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		
		UserDao dao = new UserDao();
		UserVo vo = dao.getUser(email, password);
		if(vo == null){
			//request.setAttribute("result", "fail");
			//HttpUtil.forward(request, response, "/views/user/loginform.jsp");
			String mobile = request.getParameter("mobile");
			if("1".equals(mobile)){
				HttpUtil.redirect(request, response, "/mysite/main?result=fail");
			}else{
				HttpUtil.redirect(request, response, "/mysite/user?a=loginform&result=fail");
			}
			return;
		}

		HttpSession session = request.getSession(true);
		session.setAttribute("authUser", vo);

			
		HttpUtil.redirect(request, response, "/mysite/main" );
	}

}