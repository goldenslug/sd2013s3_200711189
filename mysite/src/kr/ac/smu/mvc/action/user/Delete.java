package kr.ac.smu.mvc.action.user;

import guestbook.GuestDao;
import guestbook.GuestVo;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;

public class Delete extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		String no = request.getParameter("no");
		String pwd = request.getParameter("pass");
		String mobile = request.getParameter("mobile");
		GuestDao dao = new GuestDao();
		GuestVo vo = new GuestVo();
		vo.setNo(Integer.parseInt(no));
		vo.setPass(pwd);
		dao.delete(vo);
		
		HttpUtil.forward(request, response, "/views/guestbook/list.jsp");
		
	}

}
