package kr.ac.smu.mvc.action.user;

import guestbook.GuestDao;
import guestbook.GuestVo;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;

public class Add extends Action 
{

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException 
			{
		String mobile = request.getParameter("mobile");
		String name = request.getParameter("name");
		String pwd = request.getParameter("pass");
		String memo= request.getParameter("memo");

		GuestVo vo = new GuestVo();
		GuestDao dao = new GuestDao();
		vo.setName(name);
		vo.setPass(pwd);
		vo.setMemo(memo);
		System.out.println(mobile);
		dao.insert(vo);
		HttpUtil.forward(request, response,"/views/guestbook/list.jsp");
			}

}

