package kr.ac.smu.mvc.action.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import board.BoardDao;
import board.BoardVo;

public class UpdateAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String subject = request.getParameter("subject");
		String board = request.getParameter("board");
		String no = request.getParameter("no");
		
		System.out.println(subject);
		System.out.println(board);
		System.out.println(no);
		
		
		
		BoardVo vo = new BoardVo();
		BoardDao dao = new BoardDao();
		vo.setNo(Integer.parseInt(no));
		vo.setBoard(board);
		vo.setSubject(subject);
		dao.update(vo);
		HttpUtil.forward(request, response, "/views/board/boardform.jsp");
	}

}
