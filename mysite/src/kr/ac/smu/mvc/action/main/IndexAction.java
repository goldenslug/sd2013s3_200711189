package kr.ac.smu.mvc.action.main;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;

public class IndexAction extends Action {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpUtil.forward(request, response, "/views/main/index.jsp");
	}
}
