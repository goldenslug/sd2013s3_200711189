package kr.ac.smu.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.ac.smu.mvc.Action;
import kr.ac.smu.mvc.HttpUtil;
import kr.ac.smu.mvc.action.user.Add;
import kr.ac.smu.mvc.action.user.Delete;
import kr.ac.smu.mvc.action.user.DeleteForm;
import kr.ac.smu.mvc.action.user.Header;
import kr.ac.smu.mvc.action.user.JoinAction;
import kr.ac.smu.mvc.action.user.JoinSuccessAction;
import kr.ac.smu.mvc.action.user.JoinformAction;
import kr.ac.smu.mvc.action.user.ListForm;
import kr.ac.smu.mvc.action.user.LoginAction;
import kr.ac.smu.mvc.action.user.LoginformAction;
import kr.ac.smu.mvc.action.user.LogoutAction;
import kr.ac.smu.mvc.action.user.Main;
import kr.ac.smu.mvc.action.user.UpdateAction;
import kr.ac.smu.mvc.action.user.UpdateForm;

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String a = request.getParameter("a");
		Action action =null;
		System.out.println("넘오오냐고!");
		if("joinform".equals(a)){
			action = new JoinformAction();
		}else if("join".equals(a)){
			action = new JoinAction();
		}else if("joinsuccess".equals(a)){
			action = new JoinSuccessAction();
		}else if("loginform".equals(a)){
			action = new LoginformAction();
		}else if("login".equals(a)){
			action = new LoginAction();
		}else if("logout".equals(a)){
			action = new LogoutAction();
		}else if("main".equals(a)){
			action = new Main();
		}else if("header".equals(a)){
			action = new Header();
		}
		
		//방명록
		else if("add".equals(a)){
			action = new Add();
		}else if("deleteform".equals(a)){
			action = new DeleteForm();
		}else if("delete".equals(a)){
			action = new Delete();
		}else if("logout".equals(a)){
			action = new LogoutAction();
		}else if("listform".equals(a)){
			action = new ListForm();
		}
		//게시판
		
		//수정
		else if("updateform".equals(a)){
			action = new UpdateForm();
		}else if("updateaction".equals(a)){
			action = new UpdateAction();
		}else{
			HttpUtil.redirect(request,response, "/mysite/main");
			return ;
		}
		
		action.execute(request, response);
	}

}
