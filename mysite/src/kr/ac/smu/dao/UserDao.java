package kr.ac.smu.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import kr.ac.smu.vo.UserVo;

public class UserDao {
	
	public UserVo getUser(String email , String password){
		
		UserVo vo = null;
		Connection conn=null;
		PreparedStatement pstmt=null;
		try{
			//1. 드라이버 라이브러리 로드
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2. 컨넥션 얻어보기
			String dbUrl = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dbUrl,"dev","dev");


			//3. JDBC로 sql준비
			String sql=
					"select * from member where email=? and password=?";
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, email);
			pstmt.setString(2, password);

			//5. 쿼리 실행
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()){
				vo = new UserVo();
				vo.setNo(rs.getInt(1));
				vo.setName(rs.getString(2));
				vo.setEmail(rs.getString(3));
			}

		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(SQLException ex){}
			try{
				if(conn!=null){
					conn.close();
				}

			}catch(SQLException ex){}
		}
		return vo;
	}

	public void insert(UserVo vo){
		Connection conn=null;
		PreparedStatement pstmt=null;
		try{
			//1. 드라이버 라이브러리 로드
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2. 컨넥션 얻어보기
			String dbUrl = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dbUrl,"dev","dev");


			//3. JDBC로 sql준비
			String sql=
					"insert into member values(member_no_seq.nextval,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);

			//4. 컬럼값 세팅
			pstmt.setString(1, vo.getName());
			pstmt.setString(2, vo.getEmail());
			pstmt.setString(3, vo.getPassword());
			pstmt.setString(4, vo.getGender());
			

			//5. 쿼리 실행
			pstmt.executeUpdate();


		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(SQLException ex){}
			try{
				if(conn!=null){
					conn.close();
				}

			}catch(SQLException ex){}
		}
	}
}
