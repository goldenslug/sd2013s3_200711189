package guestbook;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class GuestDao {
	public void insert(GuestVo vo){
		Connection conn=null;
		PreparedStatement pstmt=null;
		try{
			//1. 드라이버 라이브러리 로드
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2. 컨넥션 얻어보기
			String dbUrl = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dbUrl,"dev","dev");


			//3. JDBC로 sql준비
			String sql=
					"insert into guestbook values(guestbook_no_seq.nextval,?,?,?,sysdate)";
			pstmt = conn.prepareStatement(sql);

			//4. 컬럼값 세팅
			pstmt.setString(1, vo.getName());
			pstmt.setString(2, vo.getPass());
			pstmt.setString(3, vo.getMemo());
			

			//5. 쿼리 실행
			pstmt.executeUpdate();


		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(SQLException ex){}
			try{
				if(conn!=null){
					conn.close();
				}

			}catch(SQLException ex){}
		}
	}
	public List<GuestVo> getList(){
		List<GuestVo> list = new ArrayList<GuestVo>();
		
		Connection conn=null;
		Statement stmt=null;
		try{
			//1. 드라이버 라이브러리 로드
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2. 컨넥션 얻어보기
			String dbUrl = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dbUrl,"dev","dev");

			//3. statement 생성
			stmt=conn.createStatement();

			//4. sql문 날리기
			String sql="select no,name,message,to_char(regdate, 'yyyy-mm-dd am hh:mi:ss') from guestbook order by regdate desc";
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()){
				GuestVo vo = new GuestVo();
				vo.setNo(rs.getInt(1));
				vo.setName(rs.getString(2));
				vo.setMemo(rs.getString(3));
				vo.setDate(rs.getString(4));
				list.add(vo);
			}

			rs.close();

		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(stmt!=null){
					stmt.close();
				}
			}catch(SQLException ex){}
			try{
				if(conn!=null){
					conn.close();
				}

			}catch(SQLException ex){}
		}
		return list;
	}
	
	public void delete(GuestVo vo){
		Connection conn=null;
		PreparedStatement pstmt=null;
		try{
			//1. 드라이버 라이브러리 로드
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//2. 컨넥션 얻어보기
			String dbUrl = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			conn = DriverManager.getConnection(dbUrl,"dev","dev");


			//3. JDBC로 sql준비
			String sql=
					"delete from guestbook where no=? and password=?";
			pstmt = conn.prepareStatement(sql);

			//4. 컬럼값 세팅
			pstmt.setInt(1, vo.getNo());
			pstmt.setString(2, vo.getPass());
			

			//5. 쿼리 실행
			pstmt.executeUpdate();


		}catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}catch(SQLException ex){
			ex.printStackTrace();
		}finally{
			try{
				if(pstmt!=null){
					pstmt.close();
				}
			}catch(SQLException ex){}
			try{
				if(conn!=null){
					conn.close();
				}

			}catch(SQLException ex){}
		}
	}
	
}
