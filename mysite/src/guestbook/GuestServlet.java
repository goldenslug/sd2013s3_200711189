package guestbook;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GuestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost( request, response );

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding( "utf-8" );

		String action = request.getParameter("a");

		if( "add".equals( action ) ){

			// 인서트 수행
			String name = request.getParameter("name");
			String pwd = request.getParameter("pwd");
			String memo= request.getParameter("content");

			GuestVo vo = new GuestVo();
			GuestDao dao = new GuestDao();
			vo.setName(name);
			vo.setPass(pwd);
			vo.setMemo(memo);

			dao.insert(vo);

			response.sendRedirect("/GuestBookMVC/gb?a=index");

		} else if("delete".equals(action)){
			
			String no = request.getParameter("no");
			String pwd = request.getParameter("pwd");
			GuestDao dao = new GuestDao();
			GuestVo vo = new GuestVo();
			vo.setNo(Integer.parseInt(no));
			vo.setPass(pwd);
			dao.delete(vo);
			response.sendRedirect("/GuestBookMVC/gb?a=index");
			
		}else if("deleteform".equals(action)){
			forwarding(request, response, "/view/deleteform.jsp");
		}
		
		else{
			forwarding(request, response, "/view/index.jsp");
		}
	}
	private void forwarding(HttpServletRequest request, HttpServletResponse response, String url ) throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher( url );
		dispatcher.forward( request, response );
	}
}
